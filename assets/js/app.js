import css from "../css/app.css"
import "phoenix_html"
import socket from "./socket"

function game() {
  socket.connect()

  let rock = document.querySelector("#rock")
  let paper = document.querySelector("#paper")
  let scissors = document.querySelector("#scissors")
  let cont = document.querySelector("#continue")
  rock.addEventListener("click", event => {
    channel.push("new_msg", {shape: "rock"})
  })
  paper.addEventListener("click", event => {
    channel.push("new_msg", {shape: "paper"})
  })
  scissors.addEventListener("click", event => {
    channel.push("new_msg", {shape: "scissors"})
  })
  cont.addEventListener("click", event => {
    let buttons = document.querySelector("#buttons")
    let result = document.querySelector("#end-round")
    buttons.style.display = "block"
    result.style.display = "none"
  })
  
  let channel = socket.channel("room:" + window.roomId, {username: window.username})
  channel.join()
    .receive("ok", resp => { console.log("Joined successfully", resp) })
    .receive("error", resp => { console.log("Unable to join", resp) })
  
  channel.on("end_round", payload => {
    let buttons = document.querySelector("#buttons")
    let result = document.querySelector("#end-round")
    let msg = result.querySelector("#message")
    buttons.style.display = "none"
    result.style.display = "block"
    if (payload.result == "draw") {
      msg.innerHTML = `A draw`
    } else if (payload.result == "win" && payload.username == window.username) {
      msg.innerHTML = `You win`
    } else if (payload.result == "win") {
      msg.innerHTML = `${payload.username} wins`
    }
  })

  channel.on("end_game", payload => {
    let buttons = document.querySelector("#buttons")
    let result = document.querySelector("#end-game")
    let msg = result.querySelector("#message")
    buttons.style.display = "none"
    result.style.display = "block"
    msg.innerHTML = `${payload.rounds}`
  })
}

if (window.roomId && window.username) game()
