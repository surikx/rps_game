FROM        erlang:21.2-alpine

RUN         apk update && \
            apk add bash

ENV         PORT=4000 \
            REPLACE_OS_VARS=true

EXPOSE      4000
WORKDIR     /opt/rps_game

ENTRYPOINT  ./bin/rps_game foreground

ADD         "_build/prod/rel/rps_game/releases/*/rps_game.tar.gz" \
            "/opt/rps_game/"
