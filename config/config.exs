use Mix.Config

config :rps_game, RpsGameWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "U8nIEBH1BOtOmatHNn0IloYNcYopceo2qWaSQs6Oul0erO3pYR6KAypfbmJdl5jf",
  render_errors: [view: RpsGameWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: RpsGame.PubSub, adapter: Phoenix.PubSub.PG2]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason

import_config "#{Mix.env()}.exs"
