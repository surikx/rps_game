use Mix.Config

config :rps_game, RpsGameWeb.Endpoint,
  http: [port: 4002],
  server: false

config :logger, level: :warn
