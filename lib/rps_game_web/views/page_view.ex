defmodule RpsGameWeb.PageView do
  use RpsGameWeb, :view

  
  def render("_rounds.html", %{game: game}) do
    names = RpsGame.Game.names(game)
    rounds = normalize_rounds(game, names)
    rounds = Enum.zip(1..length(game.rounds), rounds)
    render("rounds.html", names: names, rounds: rounds)
    |> Phoenix.HTML.safe_to_string()
  end

  defp normalize_rounds(game, names) do
    game.rounds
    |> Enum.map(
      fn {verdict, owner_shape, player_shape} ->
        verdict = case verdict do
          :draw -> "Draw"
          :owner -> "#{names.owner} won"
          :player-> "#{names.player} won"
        end
        %{verdict: verdict, owner_shape: owner_shape, player_shape: player_shape}
      end)
    |> Enum.reverse()
  end
end
