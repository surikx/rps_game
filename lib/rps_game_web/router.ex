defmodule RpsGameWeb.Router do
  use RpsGameWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RpsGameWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/login", PageController, :login
    get "/leaderboard", PageController, :leaderboard
    get "/game", PageController, :newgame
    get "/game/:id", PageController, :game

    post "/login", PageController, :post_login
  end

  # Other scopes may use custom stacks.
  # scope "/api", RpsGameWeb do
  #   pipe_through :api
  # end
end
