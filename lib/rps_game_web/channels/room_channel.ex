defmodule RpsGameWeb.RoomChannel do
  use Phoenix.Channel

  @impl true
  def join("room:" <> room_id, params, socket) do
    username = params["username"]
    socket = socket
             |> assign(:username, username)
             |> assign(:room_id, room_id)
             |> assign(:timer_ref, inactivity_timer())
    if RpsGame.Room.member?(room_id, username) do
      {:ok, socket}
    else
      {:error, %{reason: "not found"}}
    end
  end

  @impl true
  def handle_in("new_msg", %{"shape" => shape}, socket) do
    :erlang.cancel_timer(socket.assigns.timer_ref)
    result = RpsGame.Room.round(socket.assigns.room_id, socket.assigns.username, shape)
    case result do
      {:end_round, {:draw, _, _}} ->
        broadcast!(socket, "end_round", %{result: :draw, username: :draw})
      {:end_round, {username, _, _}} ->
        broadcast!(socket, "end_round", %{result: :win, username: username})
      {:end_game, game} ->
        rounds = RpsGameWeb.PageView.render("_rounds.html", game: game)
        broadcast!(socket, "end_game", %{rounds: rounds})
      _ -> :more
    end
    socket = assign(socket, :timer_ref, inactivity_timer())
    {:noreply, socket}
  end

  @impl true
  def handle_info({:timeout, _, :inactive}, socket) do
    shape = Enum.random(["rock", "paper", "scissors"])
    handle_in("new_msg", %{"shape" => shape}, socket)
  end

  defp inactivity_timer() do
    :erlang.start_timer(5000, self(), :inactive, [])
  end
end
