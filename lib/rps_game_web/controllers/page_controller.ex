defmodule RpsGameWeb.PageController do
  use RpsGameWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def login(conn, _params) do
    render(conn, "login.html")
  end

  def post_login(conn, params) do
    username = params["name"]
    password = params["password"]
    with :ok <- RpsGame.User.login(username, password) do
      put_session(conn, :username, username)
      |> redirect(to: "/")
    else
      {:error, :wrong_password} -> 
        conn
        |> put_flash(:error, "Wrong password.")
        |> render("login.html")
      {:error, :not_found} ->
        RpsGame.User.new(username, password)
        put_session(conn, :username, username)
        |> redirect(to: "/")
    end
  end

  def leaderboard(conn, _params) do
    board = RpsGame.Leaderboard.get()
    render(conn, "leaderboard.html", board: board)
  end

  def newgame(conn, _params) do
    username = Plug.Conn.get_session(conn, :username)
    id = RpsGame.Room.new(username)
    redirect(conn, to: "/game/#{id}")
  end

  def game(conn, %{"id" => id}) do
    username = Plug.Conn.get_session(conn, :username)
    with :ok <- RpsGame.Room.add(id, username) do
      render(conn, "game.html", room_id: id)
    else
      {:error, :not_found} -> redirect(conn, to: "/")
    end
  end
end
