defmodule RpsGame.Room.Worker do
  @moduledoc """
  Main worker process for room
  """

  use GenServer

  require Logger

  alias RpsGame.Game
  alias RpsGame.Leaderboard

  # if no activity within this time room will be stopped
  @cleanup_timeout 30000

  def start_link(args, opts) do
    username = opts[:username]
    GenServer.start_link(__MODULE__, args ++ [owner: username], opts)
  end

  def add(id, username) do
    GenServer.call(id, {:add, username})
  end

  def member?(id, username) do
    GenServer.call(id, {:member?, username})
  end

  def names(id) do
    GenServer.call(id, :names)
  end

  def round(id, username, shape) do
    GenServer.call(id, {:round, username, shape})
  end

  def child_spec(arg) do
    %{id: __MODULE__, 
      restart: :temporary,
      start: {__MODULE__, :start_link, [arg]}}
  end

  @impl true
  def init([owner: username]) do
    state = %{owner: username, game: nil}
    {:ok, state}
  end

  @impl true
  def handle_call({:add, username}, _from, %{owner: username} = state) do
    {:reply, :ok, state, @cleanup_timeout}
  end

  def handle_call({:add, username}, _from, %{owner: owner, game: nil} = state) do
    game = Game.new(owner, username)
    {:reply, :ok, Map.put(state, :game, game), @cleanup_timeout}
  end

  def handle_call({:add, _username}, _from, state) do
    {:reply, {:error, :not_found}, state, @cleanup_timeout}
  end

  def handle_call({:member?, username}, _from, %{owner: username} = state) do
    {:reply, true, state, @cleanup_timeout}
  end

  def handle_call({:member?, username}, _from, %{game: game} = state) do
    {:reply, Game.member?(game, username), state, @cleanup_timeout}
  end

  def handle_call(:names, _from, %{game: nil} = state) do
    {:reply, [], state, @cleanup_timeout}
  end

  def handle_call(:names, _from, %{game: game} = state) do
    names = Game.names(game)
    {:reply, names, state, @cleanup_timeout}
  end

  def handle_call({:round, _username, _shape}, _from, %{game: nil} = state) do
    {:reply, :ok, state, @cleanup_timeout}
  end

  def handle_call({:round, username, shape}, _from, %{game: game} = state) do
    game = Game.round(game, username, shape)
    state = Map.put(state, :game, game)
    case game.state do
      :more -> {:reply, game.state, state, @cleanup_timeout}
      :end_round -> 
        round = case List.first(game.rounds) do
          {:draw, _, _} = draw -> draw
          {:owner, shape1, shape2}  -> {game.owner, shape1, shape2}
          {:player, shape1, shape2}  -> {game.player, shape1, shape2}
        end
        {:reply, {game.state, round}, state, @cleanup_timeout}
      :end_game -> 
        Leaderboard.add_scores(game)
        {:stop, :normal, {game.state, game}, state}
    end
  end

  @impl true
  def handle_info(:timeout, state) do
    {:stop, :normal, state}
  end
end
