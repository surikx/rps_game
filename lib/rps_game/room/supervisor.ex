defmodule RpsGame.Room.Supervisor do
  use DynamicSupervisor

  def start_link(arg) do
    DynamicSupervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def start_child(name, username) do
    spec = {RpsGame.Room.Worker, name: name, username: username}
    DynamicSupervisor.start_child(__MODULE__, spec)
  end

  @impl true
  def init(initial_arg) do
    DynamicSupervisor.init(
      strategy: :one_for_one,
      extra_arguments: [initial_arg]
    )
  end
end
