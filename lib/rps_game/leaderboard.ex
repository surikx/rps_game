defmodule RpsGame.Leaderboard do
  @moduledoc """
  This module handels global leaderboard.

  For now this is only simple ets storage without any clever sorting/selecting methods.
  """

  use GenServer

  require Logger

  alias RpsGame.Game

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  # this needs clever sorting, now only shows the first 10
  def get(limit \\ 10) do
    :ets.tab2list(__MODULE__)
    |> Enum.take(limit)
  end

  def add_scores(%Game{rounds: rounds} = game) do
    Enum.reduce(rounds, %{owner: 0, player: 0}, 
                fn 
                  {:owner, _, _}, %{owner: count} = acc ->
                    %{acc | owner: count + 1}
                  {:player, _, _}, %{player: count} = acc ->
                    %{acc | player: count + 1}
                  _, acc -> acc
                end)
                |> add_score(game)
    
  end

  defp add_score(%{owner: owner, player: player}, game) when owner > player, do:
    GenServer.cast(__MODULE__, {:add_win, game.owner, game.player})

  defp add_score(%{owner: owner, player: player}, game) when owner < player, do:
    GenServer.cast(__MODULE__, {:add_win, game.player, game.owner})

  defp add_score(_, game), do:
    GenServer.cast(__MODULE__, {:add_draw, game.player, game.owner})

  @impl true
  def init(_args) do
    :ets.new(__MODULE__, [:protected, :named_table, read_concurrency: true])
    {:ok, nil}
  end

  @impl true
  def handle_call(_msg, _from, state) do
    {:reply, :ok, state}
  end

  @impl true
  def handle_cast({:add_win, winner, loser}, state) do
    insert(winner, :win)
    insert(loser, :lose)
    {:noreply, state}
  end

  def handle_cast({:add_draw, player1, player2}, state) do
    insert(player1, :lose)
    insert(player2, :lose)
    {:noreply, state}
  end

  defp insert(username, type) do
    result = case :ets.lookup(__MODULE__, username) do
      [{^username, games, wins}] -> 
        new_score(username, games, wins, type)
       _ -> new_score(username, 0, 0, type)
    end
    :ets.insert(__MODULE__, result)
  end

  defp new_score(username, games, wins, :win), do: {username, games+1, wins+1}
  defp new_score(username, games, wins, :lose), do: {username, games+1, wins}
end
