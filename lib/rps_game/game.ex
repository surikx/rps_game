defmodule RpsGame.Game do
  @moduledoc """
  This module contains game logic implementation.

  It allows two player to play Rock Paper Scissors game 
  for N rounds and then provides result table.
  """

  defstruct owner: nil,
            owner_shape: nil,
            player: nil,
            player_shape: nil,
            state: :more,
            round: 1,
            max_rounds: 10,
            rounds: []

  alias __MODULE__, as: Game

  def new(owner, player \\ nil, opts \\ [rounds: 2])

  def new(owner, player, opts) do
    %Game{owner: owner, player: player, max_rounds: opts[:rounds]}
  end

  def member?(%Game{owner: name}, name), do: true
  def member?(%Game{player: name}, name), do: true
  def member?(_, _), do: false

  def names(%Game{owner: owner, player: player}), do: %{owner: owner, player: player}

  def round(%Game{} = game, username, shape) 
   when shape in ["paper", "rock", "scissors"] and
        is_binary(username) do
          do_round(game, username, shape)
  end

  def round(_, _, _), do: raise ArgumentError

  def do_round(%Game{state: :end_game} = game, _username, _shape), do: game

  def do_round(%Game{owner: username} = game, username, shape) do
    evaluate_round(%Game{game | owner_shape: shape})
  end

  def do_round(%Game{player: username} = game, username, shape) do
    evaluate_round(%Game{game | player_shape: shape})
  end

  defp evaluate_round(%Game{owner_shape: owner_shape, player_shape: player_shape} = game) do
    result = do_evaluate_round(owner_shape, player_shape)
    do_evaluate_game(game, result)
  end
  
  defp do_evaluate_game(%Game{} = game, :more) do
    %Game{game | state: :more}
  end

  defp do_evaluate_game(%Game{owner_shape: owner_shape, 
                              player_shape: player_shape, 
                              round: round, 
                              rounds: rounds} = game, 
                              result) 
   when result in [:draw, :owner, :player] do
    last_round = {result, owner_shape, player_shape}
    rounds = [last_round | rounds]
    game = %Game{
      game |
      owner_shape: nil, 
      player_shape: nil,
      round: round + 1,
      rounds: rounds
    }
    if game.round > game.max_rounds, 
      do: %Game{game | state: :end_game},
      else: %Game{game | state: :end_round}
  end

  defp do_evaluate_round(nil, _), do: :more
  defp do_evaluate_round(_, nil), do: :more
  defp do_evaluate_round(shape, shape), do: :draw
  defp do_evaluate_round(shape, shape), do: :draw
  defp do_evaluate_round("rock", "scissors"), do: :owner
  defp do_evaluate_round("rock", "paper"), do: :player
  defp do_evaluate_round("scissors", "rock"), do: :player
  defp do_evaluate_round("scissors", "paper"), do: :owner
  defp do_evaluate_round("paper", "rock"), do: :owner
  defp do_evaluate_round("paper", "scissors"), do: :player
end
