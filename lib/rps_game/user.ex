defmodule RpsGame.User do
  @moduledoc """
  User storage which uses ets for now.
  """

  use GenServer

  require Logger

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  def new(name, password) do
    case login(name, password) do
      :ok -> :ok
      {:error, :not_found} ->
        GenServer.call(__MODULE__, {:new, name, password})
      _ -> {:error, :user_exists}
    end
  end

  def login(name, password) do
    case :ets.lookup(__MODULE__, name) do
      [{^name, ^password}] -> :ok
      [] -> {:error, :not_found}
      _ -> {:error, :wrong_password}
    end
  end

  @impl true
  def init(_args) do
    :ets.new(__MODULE__, [:protected, :named_table, read_concurrency: true])
    {:ok, nil}
  end

  @impl true
  def handle_call({:new, name, password}, _from, state) do
    :ets.insert(__MODULE__, {name, password})
    {:reply, :ok, state}
  end

  def handle_call(_msg, _from, state) do
    {:reply, :ok, state}
  end
end
