defmodule RpsGame.Room do
  @moduledoc """
  API for room managment. See `RpsGame.Room.Worker` for details.
  """

  require Logger

  def start_link(_args) do
    Registry.start_link(keys: :unique, name: __MODULE__)
  end

  def child_spec(opts) do
    %{id: __MODULE__, start: {__MODULE__, :start_link, [opts]}, type: :supervisor}
  end

  def new(nil), do: nil
  def new(username) do
    id = <<
      System.system_time(:nanosecond)::64,
      :erlang.phash2({node(), self()}, 16_777_216)::24,
      :erlang.unique_integer()::32
    >> |> Base.hex_encode32(case: :lower)
    via(id)
    |> RpsGame.Room.Supervisor.start_child(username)
    id
  end

  def add(id, username) do
    via(id)
    |> RpsGame.Room.Worker.add(username)
  end

  def member?(id, username) do
    via(id)
    |> RpsGame.Room.Worker.member?(username)
  end
  
  def round(id, username, shape) 
   when shape in["rock", "paper", "scissors"] do
    via(id)
    |> RpsGame.Room.Worker.round(username, shape)
  end

  def names(id) do
    via(id)
    |> RpsGame.Room.Worker.names()
  end

  defp via(id), do: {:via, Registry, {__MODULE__, id}}
end
