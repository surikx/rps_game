defmodule RpsGame.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      RpsGameWeb.Endpoint,
      RpsGame.User,
      RpsGame.Leaderboard,
      RpsGame.Room,
      RpsGame.Room.Supervisor
    ]

    # for Plug.Session
    :ets.new(:session, [:named_table, :public, read_concurrency: true])

    opts = [strategy: :one_for_one, name: RpsGame.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    RpsGameWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
