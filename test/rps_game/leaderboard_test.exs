defmodule RpsGame.LeaderboardTest do
  use ExUnit.Case

  alias RpsGame.Leaderboard
  alias RpsGame.Game

  setup_all do
    game = Game.new("owner", "player", rounds: 2)
           |> Game.round("owner", "paper")
           |> Game.round("player", "paper")
           |> Game.round("owner", "scissors")
           |> Game.round("player", "paper")
    Leaderboard.add_scores(game)
    game = Game.new("owner", "player", rounds: 2)
           |> Game.round("owner", "paper")
           |> Game.round("player", "paper")
           |> Game.round("owner", "paper")
           |> Game.round("player", "paper")
    Leaderboard.add_scores(game)
  end

  test "Leaderboards has proper information about games" do
    assert Leaderboard.get() == [{"player", 2, 0}, {"owner", 2, 1}]
  end
end
