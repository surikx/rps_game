defmodule RpsGame.GameTest do
  use ExUnit.Case

  alias RpsGame.Game

  setup_all do
    game = Game.new("owner", "player", rounds: 2)
    [game: game]
  end

  test "game properly created", %{game: game} do
    assert match? %Game{owner: "owner", player: "player", rounds: []}, game
  end

  test "member? returns correct value", %{game: game} do
    assert Game.member?(game, "owner")
    assert Game.member?(game, "player")
    refute Game.member?(game, "user")
  end

  test "owner wins when he starts", %{game: game} do
    game = Game.round(game, "owner", "paper")
    assert :more == game.state
    assert match? %Game{owner_shape: "paper"}, game

    game = Game.round(game, "player", "rock")
    assert :end_round == game.state
    assert [{:owner, "paper", "rock"}] == game.rounds
    assert match? %Game{owner_shape: nil, player_shape: nil}, game

    game = Game.round(game, "owner", "scissors")
    game = Game.round(game, "player", "paper")
    assert :end_game == game.state
    assert [{:owner, "paper", "rock"},
            {:owner, "scissors", "paper"}] == Enum.reverse(game.rounds)
  end

  test "player wins when he starts", %{game: game} do
    game = Game.round(game, "player", "rock")
    game = Game.round(game, "owner", "scissors")
    game = Game.round(game, "player", "rock")
    game = Game.round(game, "owner", "scissors")
    assert :end_game == game.state
    assert 2 == length(game.rounds)
  end
end
