defmodule RpsGameWeb.PageControllerTest do
  use RpsGameWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Welcome to Rock Paper Scissors Game"
  end
end
