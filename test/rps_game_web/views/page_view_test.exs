defmodule RpsGameWeb.PageViewTest do
  use RpsGameWeb.ConnCase, async: true

  test "rounds.html renders proper game results" do
    game = RpsGame.Game.new("owner", "player", [rounds: 2])
           |> RpsGame.Game.round("owner", "paper")
           |> RpsGame.Game.round("player", "rock")
           |> RpsGame.Game.round("owner", "rock")
           |> RpsGame.Game.round("player", "scissors")
    rendered = RpsGameWeb.PageView.render("_rounds.html", game: game)
    assert rendered =~ "<table>"
    assert rendered =~ "<th>Result</th>"
    assert rendered =~ "<td>1</td>"
    assert rendered =~ "<td>owner won</td>"
    assert rendered =~ "<td>paper</td>"
  end
end
