~w(rel plugins *.exs)
|> Path.join()
|> Path.wildcard()
|> Enum.map(&Code.eval_file(&1))

use Mix.Releases.Config,
    # This sets the default release built by `mix release`
    default_release: :default,
    # This sets the default environment used by `mix release`
    default_environment: Mix.env()

environment :dev do
  set dev_mode: true
  set include_erts: false
  set cookie: :"NtrDi8t];!^5;;19.=eCgAY&VSI[9Vh[VDOpGe6%*1Xiz[K_kt]i84dis?bO6<=L"
end

environment :prod do
  set include_erts: false
  set include_src: false
  set cookie: :"1?UUyc)Jf{xs(dN4b56W>jj<$t5wkRCgy8(SmZo%.W!2>~!Zh02;($`!&rl<Z6_Y"
  set vm_args: "rel/vm.args"
end

release :rps_game do
  set version: current_version(:rps_game)
  set applications: [
    :runtime_tools
  ]
end

